# genpass

To compile:

```
gcc -o genpass genpass.c
```

If you run the binary by itself it'll out put just one password. If you add a number after the binary, it'll add more.

For example:

```
genpass 5

#n+_$(FU)YPr"Yx_
H4z=%zk>G(o6W@.i
y&3@u8"$hGQYeOs]
(r8sqt]?Xa3ulyc.
b"GVA/.+Dzt+$l6<

```

Use at your own risk.
