#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#define MIN_PASSWORD_LENGTH 16

int main(int argc, char *argv[]) {
  int num_passwords = 1;  // default to 1 password

  // Check if a command line argument was provided
  if (argc > 1) {
    // Try to parse the command line argument as an integer
    int parsed_arg = atoi(argv[1]);
    // If the command line argument is a valid positive integer, use it as the number of passwords to generate
    if (parsed_arg > 0) {
      num_passwords = parsed_arg;
    }
  }

  char password[MIN_PASSWORD_LENGTH + 1];  // +1 for null terminator
  int i;

  // Create an array of characters that includes upper and lower case letters, numbers, and special characters
  char characters[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()_+-=[]{};':\"<>,.?/\\|";
  int num_characters = strlen(characters);

  srand(time(NULL));  // seed the random number generator

  for (int n = 0; n < num_passwords; n++) {
    for (i = 0; i < MIN_PASSWORD_LENGTH; i++) {
      // Generate a random number between 0 and num_characters - 1
      int random_char = rand() % num_characters;
      // Convert the random number to a character and add it to the password
      password[i] = characters[random_char];
    }

    // null-terminate the password string
    password[MIN_PASSWORD_LENGTH] = '\0';

    printf("%s\n", password);
  }

  return 0;
}
